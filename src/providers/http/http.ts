import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

  private base_url = "192.168.1.14:8000";

  constructor(public http: HttpClient) {
  }

  public getUsers(){
      return this.http.get('http://'+this.base_url+'/users');
  }

  public getUser($id){
      return this.http.get('http://'+this.base_url+'/users/'+$id);
  }

  public getCategories(){
      return this.http.get('http://'+this.base_url+'/categories');
  }

  public getCategory($id){
      return this.http.get('http://'+this.base_url+'/categories/'+$id);
  }

  public getFamilies(){
      return this.http.get('http://'+this.base_url+'/families');
  }

  public getFamily($id){
    return this.http.get('http://'+this.base_url+'/families/'+$id);
  }

  public getPlants($id){
      return this.http.get('http://'+this.base_url+'/users/'+$id+'/plants/');
  }

  public getPlant($id, $id2){
    return this.http.get('http://'+this.base_url+'/users/'+$id+'/plants/'+$id2);
  }
}
