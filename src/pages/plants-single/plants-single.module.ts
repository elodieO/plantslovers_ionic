import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantsSinglePage } from './plants-single';

@NgModule({
  declarations: [
    PlantsSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(PlantsSinglePage),
  ],
})
export class PlantsSinglePageModule {}
