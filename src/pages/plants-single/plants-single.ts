import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import {HttpProvider} from "../../providers/http/http";
import { CategoriesSinglePage } from '../categories-single/categories-single';
import { FamiliesSinglePage } from '../families-single/families-single';

/**
 * Generated class for the PlantsSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-plants-single',
  templateUrl: 'plants-single.html',
})
export class PlantsSinglePage {

  plant: any;
  private userUuid: string;
  private uuid: string;
  public name: string;
  public photo: string;
  public description: string;
  public category: any;
  public categoryName : string;
  public family: any;
  public familyName : string;
  public water : number;
  public exposure : number;
  public temperature : number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider: HttpProvider) {
    this.uuid = navParams.data.id;
    this.userUuid = navParams.data.userId;

      this.httpProvider.getPlant(this.userUuid, this.uuid).subscribe((response)=>{
          this.plant = response;
          this.name = this.plant.name;
          this.photo = this.plant.photo;
          this.description = this.plant.description;
          this.category = this.plant.category;
          this.categoryName = this.category.name;
          this.family = this.plant.family;
          this.familyName = this.family.name;
          this.water = this.plant.water;
          this.exposure = this.plant.exposure;
          this.temperature = this.plant.temperature;
      });

  }

    public categorySelected($category){
        this.navCtrl.push(CategoriesSinglePage, {
            id: $category.uuid
        });
    }

    public familySelected($family){
      this.navCtrl.push(FamiliesSinglePage, {
          id: $family.uuid
      });
  }

}
