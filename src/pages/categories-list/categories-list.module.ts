import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesListPage } from './categories-list';

@NgModule({
  declarations: [
    CategoriesListPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesListPage),
  ],
})
export class CategoriesListPageModule {}
