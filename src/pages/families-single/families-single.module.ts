import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FamiliesSinglePage } from './families-single';

@NgModule({
  declarations: [
    FamiliesSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(FamiliesSinglePage),
  ],
})
export class FamiliesSinglePageModule {}
