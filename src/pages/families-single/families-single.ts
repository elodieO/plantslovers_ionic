import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {HttpProvider} from "../../providers/http/http";
import {PlantsSinglePage} from "../plants-single/plants-single";

/**
 * Generated class for the UserSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-families-single',
  templateUrl: 'families-single.html',
})
export class FamiliesSinglePage {

  family: any;
  plants: any;

  private uuid: string;
  public name: string;
  public photo: string;
  public plantsNumber: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {

    this.uuid = navParams.data.id;
    this.httpProvider.getFamily(this.uuid).subscribe((response)=>{
          this.family = response;
          this.name = this.family.name;
          this.photo = this.family.photo;
          this.plants = this.family.plants;
          this.plantsNumber = this.plants.length;
      });

  }

}
