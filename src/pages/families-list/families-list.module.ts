import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FamiliesListPage } from './families-list';

@NgModule({
  declarations: [
    FamiliesListPage,
  ],
  imports: [
    IonicPageModule.forChild(FamiliesListPage),
  ],
})
export class FamiliesListPageModule {}
