import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { HttpProvider } from "../../providers/http/http";
import {FamiliesSinglePage} from "../families-single/families-single";


/**
 * Generated class for the UsersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-families-list',
  templateUrl: 'families-list.html',
})
export class FamiliesListPage {

    families: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {

        this.httpProvider.getFamilies().subscribe((response)=>{
            this.families = response;
        });

    }

    // public itemSelected($family){
    //     this.navCtrl.push(FamiliesSinglePage, {
    //         id: $family.uuid,
    //     });
    // }

}
