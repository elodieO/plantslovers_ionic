import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {UsersSinglePage} from "../users-single/users-single";
import { HttpProvider } from "../../providers/http/http";

/**
 * Generated class for the UsersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-users-list',
  templateUrl: 'users-list.html',
})
export class UsersListPage {

    users: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {

        this.httpProvider.getUsers().subscribe((response)=>{
            this.users = response;
        });

    }

    public itemSelected($user){
        this.navCtrl.push(UsersSinglePage, {
            id: $user.uuid
        });
    }

}
