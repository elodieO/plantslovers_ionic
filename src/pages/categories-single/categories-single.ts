import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {HttpProvider} from "../../providers/http/http";
import {PlantsSinglePage} from "../plants-single/plants-single";

/**
 * Generated class for the UserSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categories-single',
  templateUrl: 'categories-single.html',
})
export class CategoriesSinglePage {

  category: any;
  plants: any;

  private uuid: string;
  public name: string;
  public photo: string;
  public plantsNumber: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {

    this.uuid = navParams.data.id;
    this.httpProvider.getCategory(this.uuid).subscribe((response)=>{
          this.category = response;
          this.name = this.category.name;
          this.photo = this.category.photo;
          this.plants = this.category.plants;
          this.plantsNumber = this.plants.length;
      });

  }

}
