import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesSinglePage } from './categories-single';

@NgModule({
  declarations: [
    CategoriesSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesSinglePage),
  ],
})
export class CategoriesSinglePageModule {}
