import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { HttpProvider } from "../../providers/http/http";
import {PlantsSinglePage} from "../plants-single/plants-single";


/**
 * Generated class for the UsersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-plants-list',
  templateUrl: 'plants-list.html',
})
export class PlantsListPage {

    plants: any;
    private uuid = "c0f21e7f-f8bf-11e8-8881-107b44dbc52f" ;

    constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {

        this.httpProvider.getPlants(this.uuid).subscribe((response)=>{
            this.plants = response;
        });

    }

    public itemSelected($plant){
        this.navCtrl.push(PlantsSinglePage, {
            id: $plant.uuid,
            userId: this.uuid
        });
    }

}
