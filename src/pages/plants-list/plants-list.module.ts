import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantsListPage } from './plants-list';

@NgModule({
  declarations: [
    PlantsListPage,
  ],
  imports: [
    IonicPageModule.forChild(PlantsListPage),
  ],
})
export class PlantsListPageModule {}
