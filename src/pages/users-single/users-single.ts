import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {PlantsSinglePage} from "../plants-single/plants-single";
import {HttpProvider} from "../../providers/http/http";

/**
 * Generated class for the UserSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-users-single',
  templateUrl: 'users-single.html',
})
export class UsersSinglePage {

  user: any;
  plants: any;

  private uuid: string;
  public name: string;
  public mail: string;
  public photo: string;
  public plantsNumber: number;



  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public httpProvider: HttpProvider) {
    // this.uuid = navParams.data.id;
    this.uuid = "c0f21e7f-f8bf-11e8-8881-107b44dbc52f";

      this.httpProvider.getUser(this.uuid).subscribe((response)=>{
          this.user = response;
          this.name = this.user.name;
          this.mail = this.user.mail;
          this.photo = this.user.photo;

      });

      this.httpProvider.getPlants(this.uuid).subscribe((response)=>{
          this.plants = response;
          this.plantsNumber = this.plants.length;
      });

  }

    public itemSelected($plant){
        this.navCtrl.push(PlantsSinglePage, {
            id: $plant.uuid,
            userId: this.uuid
        });
    }


}
