import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';

import {UsersSinglePage} from "../users-single/users-single";
import {PlantsListPage} from "../plants-list/plants-list";
import { FamiliesListPage } from '../families-list/families-list';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PlantsListPage;
  tab2Root = FamiliesListPage;
  tab3Root = UsersSinglePage;

  constructor() {

  }
}
