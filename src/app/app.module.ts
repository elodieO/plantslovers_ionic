import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { UsersListPage } from '../pages/users-list/users-list';
import { UsersSinglePage } from '../pages/users-single/users-single';
import { PlantsSinglePage } from '../pages/plants-single/plants-single';
import { PlantsListPage } from '../pages/plants-list/plants-list';
import { FamiliesListPage } from '../pages/families-list/families-list';
import { FamiliesSinglePage } from '../pages/families-single/families-single';
import { CategoriesSinglePage } from '../pages/categories-single/categories-single';

// import { UserSinglePage } from '../pages/users-single/users-single';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpProvider } from '../providers/http/http';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    UsersListPage,
    UsersSinglePage,
    PlantsSinglePage,
    PlantsListPage,
    FamiliesListPage,
    CategoriesSinglePage,
    FamiliesSinglePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    UsersListPage,
    UsersSinglePage,
    PlantsSinglePage,
    PlantsListPage,
    FamiliesListPage,
    CategoriesSinglePage,
    FamiliesSinglePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
